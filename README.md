README

Localisation moduleWifi : 
    Récupérer le fichier wifiModule disponible : https://gitlab.com/Gulian/KeyProject/blob/master/Wifitest/wifiModule/wifiModule.ino
    Il suffit de téléverser le code dans le module.
    Ce code permet de récupérer les différentes informations à propos du réseau connecté (SSID, BSSID, @IP de l'AP, @IP du module, ainsi que leur adresse MAC)
    Par rapport a l'adresse BSSID du réseau connecté, il est possible de connaitre la localisation (a une salle près) du module.