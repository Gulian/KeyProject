#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#include <stdio.h>
#include <rgb_lcd.h>

#define WIFI_AP_KAMPUS "kampus"
#define WIFI_AUTH LWIFI_OPEN  // Nature du réseau wifi "OPEN"

// @MAC de la borne WIFI
char *kampus[7][2] = {{"00:1D:A2:D8:38:B1", "Administration"},
  {"00:14:6A:F0:45:51", "Proj-doc"},
  {"00:1D:A2:69:8E:9A", "TD2/TD1"},
  {"00:1D:A2:D8:3D:E1", "Genie inf"},
  {"00:13:1A:96:7D:71", "Info prog"},
  {"00:14:6A:F0:41:31", "Physique-Appliquée"},
  {"00:14:6A:F0:45:51", "Proj-Doc"}
};

//Initialisation du serveur
LWiFiServer server(80);

//Fonction de localisation du module
rgb_lcd lcd;
void Localize(char *kampus[7][2]) {
  byte bssid[6];
  LWiFi.BSSID(bssid);                                                                                         //Récupération du BSSID sur réseau kampis
  char bssid2[6];
  sprintf(bssid2, "%02X:%02X:%02X:%02X:%02X:%02X", bssid[0], bssid[1], bssid[2],  bssid[3], bssid[4], bssid[5]); //Convertion du bssid en chaine de caractère
  Serial.print("BSSID du réseau connecté : ");
  Serial.println(bssid2);
  int i;
  for (i = 0; i < 7; i++) {
    if (strcmp(bssid2, kampus[i][0]) == 0) {                                                                  //Comparaison du BSSID récupéré au différent bssid du tableau kampus[][]
      Serial.print("Je suis en ");
      Serial.println(kampus[i][1]);
      const int ColorR = 255;
      const int ColorV = 255;
      const int ColorB = 255;
      lcd.begin(15, 2);
      lcd.setRGB(ColorR, ColorV, ColorB);
      lcd.print("Je suis en ");
      lcd.setCursor(0, 1);
      lcd.print(kampus[i][1]);
      lcd.blink();
    }
  }
}

void printWifiStatus()
{
  // Affiche le SSID du réseau :
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // Affiche l'adresse IP du module:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // Affiche le masque réseau
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  // Affiche l'adresse IP de la gateway
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // Affiche la puissance du signal
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");

  // Affiche l'addresse Mac Arduino:
  byte mac[6];
  LWiFi.macAddress(mac);
  Serial.print("MAC address du module LinkitOne: ");
  Serial.print(mac[0], HEX);
  Serial.print(":");
  Serial.print(mac[1], HEX);
  Serial.print(":");
  Serial.print(mac[2], HEX);
  Serial.print(":");
  Serial.print(mac[3], HEX);
  Serial.print(":");
  Serial.print(mac[4], HEX);
  Serial.print(":");
  Serial.println(mac[5], HEX);

}

void setup()
{
  LWiFi.begin();
  Serial.begin(115200);

  // Connexion au réseau kampus
  Serial.println("Connexion au WIFI Kampus");
  while (0 == LWiFi.connect(WIFI_AP_KAMPUS))
  {
    delay(1000);
  }
  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");
}

int loopCount = 0;
void loop()
{
  loopCount++;
  printWifiStatus();                          //Affiche différentes informations du réseaux
  Localize(kampus);
  delay(5000);
}




